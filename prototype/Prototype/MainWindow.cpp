#include "MainWindow.h"

#include <QCoreApplication>
#include <QOpenGLFunctions>

MainWindow::MainWindow(int width, int height)
    :QWindow(),
     m_childWindow(nullptr)
{
    /*
     * Each QWindow has an associated surface.
     * Setup the surface to be used by OpenGL.
     */
    QWindow::setSurfaceType(OpenGLSurface);
    QSurfaceFormat format;
    format.setDepthBufferSize(16);
    format.setStencilBufferSize(8);
    setFormat(format);

    /*
     * Set the dimensions of the window.
     */
    QWindow::setWidth(width);
    QWindow::setHeight(height);
}

void MainWindow::setChildWindow(IChildWindow *childWindow)
{
    m_childWindow = childWindow;
}

MainWindow::~MainWindow()
{
    //TODO: Andrei: Think what to do here.
}

void MainWindow::exposeEvent(QExposeEvent *event)
{
    Q_UNUSED(event)

    if (m_childWindow != nullptr)
    {
        m_childWindow->onParentWindowExposed();
    }
}

void MainWindow::resizeEvent(QResizeEvent *event)
{
    /*
     * Update the on screen size, based on the new dimenisions.
     */
    WindowDimensions wd(QWindow::size(), QWindow::devicePixelRatio());

    if (m_childWindow != nullptr)
    {
        m_childWindow->onParentWindowResized(wd);
    }
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if (m_childWindow != nullptr)
    {
        m_childWindow->onParentWindowMouseEvent(event);
    }
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    if (m_childWindow != nullptr)
    {
        m_childWindow->onParentWindowMouseEvent(event);
    }
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    if (m_childWindow != nullptr)
    {
        m_childWindow->onParentWindowMouseEvent(event);
    }
}

/*********************************************************************
 **************** Implementation of ChildWindow class ****************
 *********************************************************************/
ChildWindow::ChildWindow(RenderController& rc, WindowDimensions parentDimensions)
    :IChildWindow(&rc),
     m_renderController(rc),
     m_parentDimensions(parentDimensions)
{
    /*
     * Connect the signals need for scene graph handling.
     */
    QQuickWindow::connect(this, &QQuickWindow::sceneGraphInitialized, this, &ChildWindow::onSceneGraphInitialized);
    QQuickWindow::connect(this, &QQuickWindow::sceneGraphInvalidated, this, &ChildWindow::onSceneGraphInvalidated);

    /*
     * Set the render target of the QuickWindow, the current surface
     * (the current surface, is the surface on the main window).
     */
    QQuickWindow::setRenderTarget(0, (m_parentDimensions.size * m_parentDimensions.devicePixelRatio));
}

ChildWindow::~ChildWindow()
{
    //TODO: Andrei: Think what should be done here.
}

void ChildWindow::onParentWindowExposed()
{
    //TODO: Andrei: Set up qml bullshit.
    m_renderController.initialize();
}

void ChildWindow::onParentWindowResized(WindowDimensions parentDimensions)
{
    m_parentDimensions = parentDimensions;

    m_renderController.makeCurrent();
    QQuickWindow::setRenderTarget(0, m_parentDimensions.size * m_parentDimensions.devicePixelRatio);
    m_renderController.doneCurrent();
    QQuickWindow::setGeometry(0, 0,m_parentDimensions.size.width(), m_parentDimensions.size.height());
    //TODO: Andrei: Set dimmension of the root item
    m_renderController.render(this);

}

void ChildWindow::onParentWindowMouseEvent(QMouseEvent *event)
{
    //TODO: Andrei: Think how it should be handled by a custom widget system.
    QMouseEvent mappedEvent(event->type(), event->localPos(), event->screenPos(), event->button(), event->buttons(), event->modifiers());
    QCoreApplication::sendEvent(this, &mappedEvent);
}

void ChildWindow::onSceneGraphInitialized()
{
    QQuickWindow::setRenderTarget(0, m_parentDimensions.size * m_parentDimensions.devicePixelRatio);
}

void ChildWindow::onSceneGraphInvalidated()
{
    /*
     * Do nothing.
     */
}


/*********************************************************************
 **************** Implementation of RenderController class ***********
 *********************************************************************/
RenderController::RenderController(MainWindow& mainWindow)
    :QQuickRenderControl(),
     m_timer(),
     m_openGLContext(),
     m_childWindow(nullptr),
     m_mainWindow(m_mainWindow)
{
    /*
     * Configure the timer that will cap the refresh rate.
     */
    m_timer.setSingleShot(true);
    m_timer.setInterval(5);
    connect(&m_timer, &QTimer::timeout, this, &RenderController::onTimeout);

    /*
     * Connect the signal handlers to control the rendering
     * of the child window.
     */
    connect(this, &QQuickRenderControl::renderRequested, this, &RenderController::onRenderRequested);
    connect(this, &QQuickRenderControl::sceneChanged, this, &RenderController::onRenderRequested);
}

RenderController::~RenderController()
{
    /*
     * TODO: Andrei: Think what should be done here.
     */
}

void RenderController::initialize()
{
    /*
     * Configure the OpenGL context to render on the main window.
     */
    m_openGLContext.setFormat(m_mainWindow.format());
    m_openGLContext.create();
    m_openGLContext.makeCurrent(&m_mainWindow);
    QQuickRenderControl::initialize(&m_openGLContext);
}

void RenderController::makeCurrent()
{
    m_openGLContext.makeCurrent(&m_mainWindow);
}

void RenderController::doneCurrent()
{
    m_openGLContext.doneCurrent();
}

void RenderController::render(IChildWindow* childWindow)
{
    /*
     * Force the render loop to render a new frame.
     * This is usually controlled by the timer.
     */
    m_childWindow = childWindow;
    onTimeout();
}

QWindow* RenderController::renderWindow(QPoint *offset)
{
    if (offset)
    {
        *offset = QPoint(0, 0);
    }

    return &m_mainWindow;
}

void RenderController::onTimeout()
{
    m_openGLContext.makeCurrent(&m_mainWindow);

    QQuickRenderControl::polishItems();
    QQuickRenderControl::sync();
    QQuickRenderControl::render();

    m_childWindow->resetOpenGLState();

    m_openGLContext.functions()->glFlush();

    m_openGLContext.swapBuffers(&m_mainWindow);
}

void RenderController::onRenderRequested()
{
    if (m_timer.isActive() == false)
    {
        m_timer.start();
    }
}
