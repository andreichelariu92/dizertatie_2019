#ifndef MainWindow_INCLUDE_GUARD
#define MainWindow_INCLUDE_GUARD

#include <QWindow>
#include <QQuickWindow>
#include <QQuickRenderControl>
#include <QTimer>
#include <QOpenGLContext>


struct WindowDimensions
{
    QSize size;
    qreal devicePixelRatio;

    WindowDimensions(int argWidth, int argHeight, qreal argDevicePixelRatio)
        :size(argWidth, argHeight),
         devicePixelRatio(argDevicePixelRatio)
    {}

    WindowDimensions(QSize argSize, qreal argDevicePixelRatio)
        :size(argSize),
         devicePixelRatio(argDevicePixelRatio)
    {}
};

class IChildWindow : public QQuickWindow
{
public:
    IChildWindow(QQuickRenderControl* rc)
        :QQuickWindow(rc)
    {
    }
    virtual void onParentWindowExposed() = 0;
    virtual void onParentWindowResized(WindowDimensions parentDimensions) = 0;
    virtual void onParentWindowMouseEvent(QMouseEvent *event) = 0;
    virtual ~IChildWindow()
    {
    }
};

class MainWindow : public QWindow
{
public:
    MainWindow(int width, int height);
    void setChildWindow(IChildWindow* childWindow);
    MainWindow(const MainWindow& source) = delete;
    MainWindow& operator=(const MainWindow& source) = delete;
    MainWindow(MainWindow&& source) = delete;
    MainWindow& operator=(MainWindow&& source) = delete;
    ~MainWindow();

    WindowDimensions getWindowDimensions()
    {
        return WindowDimensions(QWindow::size(), QWindow::devicePixelRatio());
    }

protected:
    void exposeEvent(QExposeEvent *event)override;
    void resizeEvent(QResizeEvent *event)override;
    void mousePressEvent(QMouseEvent *event)override;
    void mouseReleaseEvent(QMouseEvent *event)override;
    void mouseMoveEvent(QMouseEvent *event)override;

private:
    IChildWindow* m_childWindow;
};

class RenderController;
class ChildWindow : public IChildWindow
{
public:
    ChildWindow(RenderController& rc, WindowDimensions parentDimensions);
    ChildWindow(const ChildWindow& source) = delete;
    ChildWindow& operator=(const ChildWindow& source) = delete;
    ChildWindow(ChildWindow&& source) = delete;
    ChildWindow& operator=(ChildWindow&& source) = delete;
    virtual ~ChildWindow();

    virtual void onParentWindowExposed()override;
    virtual void onParentWindowResized(WindowDimensions parentDimensions)override;
    virtual void onParentWindowMouseEvent(QMouseEvent *event)override;

public slots:
    void onSceneGraphInitialized();
    void onSceneGraphInvalidated();
private:
    RenderController& m_renderController;
    WindowDimensions m_parentDimensions;
};

class RenderController : public QQuickRenderControl
{
public:
    RenderController(MainWindow& mainWindow);
    RenderController(const RenderController& source) = delete;
    RenderController& operator=(const RenderController& source) = delete;
    RenderController(RenderController&& source) = delete;
    RenderController& operator=(RenderController&& source) = delete;
    ~RenderController();

    void initialize();
    void makeCurrent();
    void doneCurrent();
    void render(IChildWindow *childWindow);

    virtual QWindow *renderWindow(QPoint *offset) override;

public slots:
    void onTimeout();
    void onRenderRequested();

private:
    QTimer m_timer;
    QOpenGLContext m_openGLContext;
    IChildWindow* m_childWindow;
    MainWindow& m_mainWindow;
};

#endif
