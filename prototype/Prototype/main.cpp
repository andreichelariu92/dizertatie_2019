#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "MainWindow.h"
#include "QmlCompiler.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    //QQmlApplicationEngine engine;
    //engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    MainWindow mainWindow(640, 480);
    RenderController renderController(mainWindow);
    ChildWindow childWindow(renderController, mainWindow.getWindowDimensions());
    ViewManager viewManager(childWindow);

    viewManager.showView(VIEW_YES_NO);
    mainWindow.setChildWindow(&childWindow);
    mainWindow.show();

    return app.exec();
}
