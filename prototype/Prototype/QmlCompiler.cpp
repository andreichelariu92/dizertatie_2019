#include "QmlCompiler.h"


QmlLoader::QmlLoader()
    :m_cache(),
     m_engine()
{
}

QQuickItem* QmlLoader::loadQml(const QString &path)
{
    if (m_cache.find(path) != m_cache.end())
    {
        return m_cache[path];
    }

    /*
     * Create a qml component (by reading the file)
     * and wait for it to be loaded.
     */
    QQmlComponent* component = new QQmlComponent(&m_engine, path);
    //TODO: Andrei: Think if I implement a callback instead.
    while (component->isLoading())
    {
        /*
         * Busy wait.
         */
    }

    if (component->isError())
    {
        const QList<QQmlError> errorList = component->errors();
        for (const QQmlError &error : errorList)
            qWarning() << error.url() << error.line() << error;
        return nullptr;
    }

    /*
     * Create QObject from the component.
     */
    QObject* object = component->create();
    if (component->isError())
    {
        const QList<QQmlError> errorList = component->errors();
        for (const QQmlError &error : errorList)
            qWarning() << error.url() << error.line() << error;
        return nullptr;
    }

    /*
     * Convert the QObject to a QQuickItem
     * and return it to the user.
     */
    QQuickItem* output = qobject_cast<QQuickItem*>(object);
    m_cache[path] = output;

    return output;
}

QmlLoader::~QmlLoader()
{
    /*
     * TODO: ANdrei: Check if I should delete
     * the pointers in the map.
     */
}

ViewManager::ViewManager(QQuickWindow &window)
    :m_paths(),
     m_loader(),
     m_window(window)
{
    m_paths[VIEW_YES_NO] = QStringLiteral("qrc:/main.qml");
    m_paths[VIEW_GRID] = QString("grid_view.qml");
    m_paths[VIEW_DINAMIC_ANIMATIONS] = QString("dynamic_animations_view.qml");
}

void ViewManager::showView(ViewId view)
{
    /*
     * Get the path corresponding to the view
     * and try to load the view.
     */
    QString& viewPath = m_paths[view];
    QQuickItem* viewItem = m_loader.loadQml(viewPath);
    if (viewItem == nullptr)
    {
        qWarning() << "Could not load view: " << view;
        return;
    }

    /*
     * Attach the view to the window.
     */
    viewItem->setParent(m_window.contentItem());
}

ViewManager::~ViewManager()
{
}
