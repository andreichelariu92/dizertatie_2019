#ifndef QmlCompiler_INCLUDE_GUARD
#define QmlCompiler_INCLUDE_GUARD

#include <QQuickItem>
#include <QString>
#include <QQmlEngine>
#include <QQuickWindow>

#include <map>

class QmlLoader
{
public:
    QmlLoader();
    QmlLoader(const QmlLoader& source) = delete;
    QmlLoader& operator=(const QmlLoader& source) = delete;
    QmlLoader(QmlLoader&& source) = delete;
    QmlLoader& operator=(QmlLoader&& source) = delete;
    ~QmlLoader();

    QQuickItem* loadQml(const QString& path);

private:
    std::map<QString, QQuickItem*> m_cache;
    QQmlEngine m_engine;
};

enum ViewId
{
    VIEW_YES_NO = 0,
    VIEW_GRID = 1,
    VIEW_DINAMIC_ANIMATIONS = 3
};

class ViewManager
{
public:
    ViewManager(QQuickWindow& window);
    ViewManager(const ViewManager& source) = delete;
    ViewManager& operator=(const ViewManager& source) = delete;
    ViewManager(ViewManager&& source) = delete;
    ViewManager& operator=(ViewManager&& source) = delete;
    ~ViewManager();

    void showView(ViewId view);
private:
    std::map<ViewId, QString> m_paths;
    QmlLoader m_loader;
    QQuickWindow& m_window;

};

#endif
