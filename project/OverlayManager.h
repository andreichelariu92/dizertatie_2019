#ifndef OverlayManager_INCLUDE_GUARD
#define OverlayManager_INCLUDE_GUARD

#include <map>

#include "OverlayControllerTemplate.h"
#include "Label.h"
#include "Icon.h"
#include "ImageProvider.h"

class TestScreen : public Overlay::OverlayScreenTemplate
{
protected:
    virtual void onShown()override;
    virtual void onHidden()override;
public:
    TestScreen(Overlay::OverlayRenderer& renderer, Overlay::ImageProvider& provider);
};

namespace Overlay
{
enum OverlayId
{
    TEST_OVERLAY        = 0,
    YES_NO_OVERLAY      = 1,
    MENU_OVERLAY        = 2,
    ANIMATION_OVERLAY   = 3
};

class OverlayManager
{
private:
    OverlayRenderer& m_renderer;
    std::map<OverlayId, OverlayScreenTemplate*> m_controllers;
    OverlayScreenTemplate* m_currentController;

public:
    OverlayManager(OverlayRenderer& renderer, Overlay::ImageProvider& provider);
    OverlayManager(const OverlayManager& source) = delete;
    OverlayManager& operator=(const OverlayManager& source) = delete;
    OverlayManager(OverlayManager&& source) = delete;
    OverlayManager& operator=(OverlayManager&& source) = delete;
    ~OverlayManager();

    void showOverlay(OverlayId id);

};

} // namespace Overlay

#endif
