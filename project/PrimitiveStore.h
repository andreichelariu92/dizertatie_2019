#ifndef PrimitiveStore_INCLUDE_GUARD
#define PrimitiveStore_INCLUDE_GUARD

#include <stdint.h>

template<typename ObjectType, int COUNT>
class ObjectStore
{
public:
    enum ErrorCode
    {
        EC_NO_ERROR        = 0,
        EC_STORE_FULL      = 1
    };

private:
    ObjectType m_primitives[COUNT];
    uint8_t m_primitiveCount;
    ErrorCode m_lastError;

public:
    ObjectStore();
    ObjectStore(const ObjectStore& source) = delete;
    ObjectType& operator=(const ObjectStore& source) = delete;
    ObjectStore(ObjectStore&& source) = delete;
    ObjectStore& operator =(ObjectStore &&source) = delete;
    ~ObjectStore();

    ObjectType* add(const ObjectType& p);
    void clear();

    ObjectType* begin();
    ObjectType* end();
    ObjectType* last();
    ObjectType* at(uint8_t idx);

    ErrorCode getLastError()const
    {
        return m_lastError;
    }
};

template<typename Primitive, int COUNT>
ObjectStore<Primitive, COUNT>::ObjectStore()
    :m_primitives(),
     m_primitiveCount(0),
     m_lastError(ObjectStore<Primitive, COUNT>::EC_NO_ERROR)
{
}

template<typename Primitive, int COUNT>
ObjectStore<Primitive, COUNT>::~ObjectStore()
{
}

template<typename Primitive, int COUNT>
Primitive* ObjectStore<Primitive, COUNT>::add(const Primitive &p)
{
    if (m_primitiveCount == COUNT)
    {
        m_lastError = EC_STORE_FULL;
        return end();
    }

    /*
     * Rely on the copy constructor of the
     * primitive to copy the data in the interanal
     * buffer.
     */
    m_primitives[m_primitiveCount] = p;
    m_primitiveCount++;

    /*
     * Return an iterator to the added element.
     */
    return (m_primitives + (m_primitiveCount - 1));
}

template<typename Primitive, int COUNT>
void ObjectStore<Primitive, COUNT>::clear()
{
    /*
     * Just reset the flag.
     * The memory will be overridden when
     * new primitives are added.
     *
     * The iterators up to this call are invalidated!
     */
    m_primitiveCount = 0;
}

template<typename Primitive, int COUNT>
Primitive* ObjectStore<Primitive, COUNT>::begin()
{
    return m_primitives;
}

template<typename Primitive, int COUNT>
Primitive* ObjectStore<Primitive, COUNT>::end()
{
    return (m_primitives + COUNT);
}

template<typename Primitive, int COUNT>
Primitive* ObjectStore<Primitive, COUNT>::last()
{
    return (m_primitives + m_primitiveCount);
}

template<typename Primitive, int COUNT>
Primitive* ObjectStore<Primitive, COUNT>::at(uint8_t idx)
{
    return (m_primitives + idx);
}

#endif
