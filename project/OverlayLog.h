#ifndef OverlayLog_INCLUDE_GUARD
#define OverlayLog_INCLUDE_GUARD

#include <QtCore/qlogging.h>

#define OL_IS_VALID_OBJECT(object) (object.getErrorStatus() == 0)
#define OL_IS_VALID(status) ((status) == 0)

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

#define OL_PRINT_LOG(message, ...) \
    qDebug("OL:" TOSTRING(__FILE__) ":" TOSTRING(__LINE__) " " message, ##__VA_ARGS__)

//To enable debug traces, uncomment this line.
#define OL_ENABLE_DEBUG 1
#define PREFIX "EXTRALOG:"

#ifdef OL_ENABLE_DEBUG
    #define OL_PRINT_DEBUG(message, ...) OL_PRINT_LOG(PREFIX message, __VA_ARGS__)
#else
    #define OL_PRINT_DEBUG(message, ...)
#endif

#define OL_PRINT_ERROR_STATUS(status) OL_PRINT_LOG("%d", (status))

#endif
