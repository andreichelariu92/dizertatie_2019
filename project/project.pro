TEMPLATE = app

QT += qml quick
CONFIG += c++11

SOURCES += main.cpp \
    Overlay.cpp \
    DrawingPrimitives.cpp \
    OverlayRenderer.cpp \
    OverlayControllerTemplate.cpp \
    Label.cpp \
    OverlayManager.cpp \
    OpenGL_Wrapper.cpp \
    ImageProvider.cpp \
    Icon.cpp \
    TextureManager.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    Overlay.h \
    DrawingPrimitives.h \
    OverlayRenderer.h \
    PrimitiveStore.h \
    OverlayControllerTemplate.h \
    Label.h \
    OverlayManager.h \
    OpenGL_Wrapper.h \
    OverlayLog.h \
    ImageProvider.h \
    stb_image.h \
    Icon.h \
    TextureManager.h

DISTFILES +=
