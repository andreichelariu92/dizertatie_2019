#ifndef Icon_INCLUDE_GUARD
#define Icon_INCLUDE_GUARD

#include "OverlayControllerTemplate.h"
#include "ImageProvider.h"

namespace Overlay
{

class IconBuilder;

class Icon : public IOverlayWidget
{
private:
    Overlay::Image* m_image;

    /*
     * Inhibit direct creation.
     * The construction of the class will be handled
     * by the IconBuilder class.
     */
    Icon(Overlay::Image* image);
    friend class IconBuilder;

public:
    /*
     * Remove copy and move operations.
     */
    Icon(const Icon& source) = delete;
    Icon& operator=(const Icon& source) = delete;
    Icon(Icon&& source) = delete;
    Icon& operator=(Icon&& source) = delete;
    virtual ~Icon();

    Overlay::Position getPosition()const
    {
        Overlay::Position defaultPosition = {0, 0};
        return (m_image != nullptr) ? m_image->position : defaultPosition;
    }

    Overlay::Dimension getDimension()const
    {
        Overlay::Dimension defaultDimension = {0, 0};
        return (m_image != nullptr) ? m_image->dimension : defaultDimension;
    }
};

class IconBuilder
{
public:
    IconBuilder(Overlay::OverlayScreenTemplate& controller);
    /*
     * Remove copy and move operations.
     */
    IconBuilder(const IconBuilder& source) = delete;
    IconBuilder& operator=(const IconBuilder& source) = delete;
    IconBuilder(IconBuilder&& source) = delete;
    IconBuilder& operator=(IconBuilder&& source) = delete;
    ~IconBuilder();

    void setPosition(uint16_t x, uint16_t y);
    void setDimension(uint16_t w, uint16_t h);
    void setSource(const char *const source, Overlay::ImageProvider& provider);
    Overlay::Icon* buildIcon();

private:
    Overlay::Icon* m_icon;
};

}

#endif
