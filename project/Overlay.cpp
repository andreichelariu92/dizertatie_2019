#include <stdio.h>

#include "Overlay.h"
#include "OverlayRenderer.h"

using Overlay::Image;
using Overlay::Text;
using Overlay::Rectangle;
using Overlay::OverlayRenderer;

/***********************************************
 * Overlay class definition.
 ***********************************************/
PrimitiveStore::PrimitiveStore(OverlayRenderer &renderer)
    :m_imageStore(),
     m_textStore(),
     m_rectangleStore(),
     m_renderer(renderer)
{
}

PrimitiveStore::~PrimitiveStore()
{
}

Image* PrimitiveStore::allocateImage()
{
    /*
     * Try to add a new (empty) image in the store.
     * The image data will be modified by the caller.
     */
    Image img;
    Image* newImage = m_imageStore.add(img);
    if (newImage == m_imageStore.end())
    {
        printf("Cannot allocate image. Error code: %d\n.", m_imageStore.getLastError());
        newImage = nullptr;
    }

    return newImage;
}

Text* PrimitiveStore::allocateText()
{
    Text text;
    Text* newText = m_textStore.add(text);
    if (newText == m_textStore.end())
    {
        printf("Cannot allocate text. Error code: %d.\n", m_textStore.getLastError());
        newText = nullptr;
    }

    return newText;
}

Overlay::Rectangle* PrimitiveStore::allocateRectangle()
{
    Overlay::Rectangle rectangle;
    Overlay::Rectangle* newRectangle = m_rectangleStore.add(rectangle);
    if (newRectangle == m_rectangleStore.end())
    {
        printf("Cannot allocate rectangle. Error code: %d.\n", m_rectangleStore.getLastError());
        newRectangle = nullptr;
    }

    return newRectangle;
}

void PrimitiveStore::hidePrimitives()
{
}

void PrimitiveStore::drawPrimitives()
{
    m_renderer.clearPrimitives();

    m_renderer.storeImages(m_imageStore.begin(), m_imageStore.last());
    m_renderer.storeTexts(m_textStore.begin(), m_textStore.last());
    m_renderer.storeRectangles(m_rectangleStore.begin(), m_rectangleStore.last());

    m_renderer.drawPrimitives();
}
