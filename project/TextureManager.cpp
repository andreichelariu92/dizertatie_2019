#include "TextureManager.h"

using std::map;

using Overlay::Texture;
using Overlay::ImageData;
using Overlay::TextureManager;

TextureManager::TextureManager()
    :m_textures()
{
}

TextureManager::~TextureManager()
{
    //default behavior is enough
}

bool TextureManager::upload(uint16_t id, const Overlay::ImageData* imageData)
{
    /*
     * Create texture.
     */
    const Texture::WrapMode wrapMode = Texture::WM_CLAMP_TO_BORDER;
    const Texture::FilterMode filterMode = Texture::FM_LINEAR;
    const Texture::Unit unit = Texture::UNIT_ZERO;
    Texture texture(wrapMode, filterMode, unit);

    /*
     * Upload the data to the texture.
     */
    const int pixelDepth = imageData->getPixelDepth();
    Texture::PixelFormat pixelFormat = Texture::PF_LAST;
    switch (pixelDepth)
    {
    case 3:
        pixelFormat = Texture::PF_RGB;
        break;
    case 4:
        pixelFormat = Texture::PF_RGBA;
        break;
    default:
        break;
    }
    if (pixelFormat == Texture::PF_LAST)
    {
        return false;
    }
    texture.bind();
    texture.upload(pixelFormat, imageData->getWidth(), imageData->getHeight(), imageData->getData());
    texture.unbind();

    /*
     * Move the texture to the map
     */
    m_textures.insert(std::make_pair(id, std::move(texture)));
    return true;
}

Texture* TextureManager::getTexture(uint16_t id)
{
    map<uint16_t, Texture>::iterator it;
    it = m_textures.find(id);
    if (it == m_textures.end())
    {
        return nullptr;
    }
    else
    {
        //TODO: Andrei: Check if this works.
        return &it->second;
    }
}

void TextureManager::clear()
{
    m_textures.clear();
}
