#include "OverlayControllerTemplate.h"

using namespace Overlay;

OverlayScreenTemplate::OverlayScreenTemplate(OverlayRenderer &renderer)
    :m_view(renderer),
     m_widgets()
{
}

OverlayScreenTemplate::~OverlayScreenTemplate()
{
    clearWidgets();
}

void OverlayScreenTemplate::show()
{
    /*
     * Call the method implemented in the children.
     * Here the children can setup the controller state.
     */
    onShown();

    m_view.drawPrimitives();
}

void OverlayScreenTemplate::hide()
{
    /*
     * Call the method implemented in the children.
     * Here the children can perform cleanup.
     */
    onHidden();

    m_view.hidePrimitives();
}

void OverlayScreenTemplate::addWidget(IOverlayWidget *w)
{
    /*
     * The controller is now responsible for cleaning
     * up the memory used by the widget.
     */
    m_widgets.push_back(w);
}

void OverlayScreenTemplate::clearWidgets()
{
    int widgetIdx = 0;
    const int widgetCount = m_widgets.size();

    for (; widgetIdx < widgetCount; ++widgetIdx)
    {
        delete m_widgets[widgetIdx];
    }

    m_widgets.clear();
}
