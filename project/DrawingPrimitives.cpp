#include <string.h>

#include "DrawingPrimitives.h"

Overlay::Color::Color()
    :r(0),
     g(0),
     b(0),
     a(255)
{
}

Overlay::String::String(const char* argData)
    :data(),
     count(0)
{
    strncpy(data, argData, MAX_STRING_LENGTH);
    data[MAX_STRING_LENGTH] = '\0';
}
