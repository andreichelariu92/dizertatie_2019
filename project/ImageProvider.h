#ifndef ImageProvider_INCLUDE_GUARD
#define ImageProvider_INCLUDE_GUARD

#include <stdint.h>

#include <string>
#include <vector>

namespace Overlay
{

class ImageData
{
 private:
    int m_width;
    int m_height;
    int m_pixelDepth;
    unsigned char* m_data;
public:
    ImageData(int width, int height, int pixelDepth, unsigned char* data);
    ImageData(const ImageData& source) = delete;
    ImageData& operator=(const ImageData& source) = delete;
    ImageData(ImageData&& source);
    ImageData& operator=(ImageData&& source);
    ~ImageData();

    int getWidth()const
    {
        return m_width;
    }
    int getHeight()const
    {
        return m_height;
    }
    int getPixelDepth()const
    {
        return m_pixelDepth;
    }
    unsigned char* getData()const
    {
        return m_data;
    }
};


class ImageProvider
{
private:
    struct ImageEntry
    {
        ImageData image;
        std::string path;

        ImageEntry(int w, int h, int pd, unsigned char* data, const char *const p)
            :image(w, h, pd, data),
             path(p)
        {
        }
    };

    std::vector<ImageEntry> m_images;

public:
    ImageProvider();
    ImageProvider(const ImageProvider& source) = delete;
    ImageProvider& operator=(const ImageProvider& source) = delete;
    ImageProvider(ImageProvider&& source) = delete;
    ImageProvider& operator=(ImageProvider&& source) = delete;
    ~ImageProvider() = default;

    uint16_t readImage(const char *const path);
    ImageData* getImageData(uint16_t id);
};

}


#endif
