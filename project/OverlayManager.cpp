#include "OverlayManager.h"

using namespace Overlay;

TestScreen::TestScreen(OverlayRenderer& renderer, ImageProvider &provider)
    :OverlayScreenTemplate(renderer)
{
    Overlay::EmptyLabelBuilder lb(*this);
    lb.setPosition(0, 630);
    lb.setDimension(1280, 138);
    Overlay::EmptyLabel* l1 = lb.buildLabel();
    OverlayScreenTemplate::addWidget(l1);

    Overlay::EmptyLabelBuilder lb2(*this);
    lb2.setPosition(795, 0);
    lb2.setDimension(465 + 20, 630);
    Overlay::EmptyLabel* l2 = lb2.buildLabel();
    OverlayScreenTemplate::addWidget(l2);

    Overlay::IconBuilder ib(*this);
    ib.setPosition(1280 - 10 - 465, 178);
    ib.setDimension(465, 328);
    ib.setSource("c:\\dizertatie_2019\\repository_gitlab\\dizertatie_2019\\resources\\bmw2.jpg", provider);
    Overlay::Icon* icon = ib.buildIcon();
    OverlayScreenTemplate::addWidget(icon);

    Overlay::IconBuilder ib2(*this);
    ib2.setPosition(795 + 98, 67);
    ib2.setDimension(318, 75);
    ib2.setSource("c:\\dizertatie_2019\\repository_gitlab\\dizertatie_2019\\resources\\text_test.jpg", provider);
    Overlay::Icon* icon2 = ib2.buildIcon();
    OverlayScreenTemplate::addWidget(icon2);
}

void TestScreen::onShown()
{
    printf("TestScreen: onShown called\n");
}

void TestScreen::onHidden()
{
    printf("TestScreen: onHidden called\n");
}

OverlayManager::OverlayManager(OverlayRenderer &renderer, ImageProvider &provider)
    :m_renderer(renderer),
     m_controllers(),
     m_currentController(nullptr)
{
    m_controllers[Overlay::TEST_OVERLAY] = new TestScreen(m_renderer, provider);
}

OverlayManager::~OverlayManager()
{
    std::map<OverlayId, OverlayScreenTemplate*>::iterator it;
    for (it = m_controllers.begin(); it != m_controllers.end(); ++it)
    {
        delete it->second;
    }

    m_controllers.clear();
}

void OverlayManager::showOverlay(OverlayId id)
{
    if (m_currentController != nullptr)
    {
        m_currentController->hide();
    }

    m_currentController = m_controllers[id];
    m_currentController->show();
}
