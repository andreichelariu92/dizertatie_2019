#include <stdio.h>

#include "OverlayRenderer.h"

using Overlay::Image;
using Overlay::Text;
using Overlay::Rectangle;
using Overlay::OverlayRenderer;

const char*  OverlayRenderer::m_vertexShaderCode = "\n"
"#version 100\n"
"attribute vec3 position;\n"
"attribute vec2 a_texturePosition;\n"
"uniform mat4 model;\n"
"uniform mat4 projection;\n"
"varying vec2 v_texturePosition;\n"
"void main()\n"
"{\n"
"gl_Position = projection * model * vec4(position.x, position.y, position.z, 1.0);\n"
"v_texturePosition = a_texturePosition;\n"
"}";


const char* OverlayRenderer::m_fragmentShaderCode = "\n"
"#version 100\n"
"precision mediump float;\n"
"uniform vec4 fragmentColor;\n"
"uniform sampler2D u_sampler;\n"
"varying vec2 v_texturePosition;\n"
"void main()\n"
"{\n"
"gl_FragColor = fragmentColor * texture2D(u_sampler, v_texturePosition);\n"
"}";

GLfloat OverlayRenderer::m_vertices[] = {
    // x      y      z      s      t
    0.0f,  0.0f,  0.0f,  0.0f,  0.0f,
    0.0f,  1.0f,  0.0f,  0.0f,  1.0f,
    1.0f,  1.0f,  0.0f,  1.0f,  1.0f,
    1.0f,  0.0f,  0.0f,  1.0f,  0.0f
};

unsigned int OverlayRenderer::m_indices[] = {
    0, 1, 2,
    0, 2, 3
};

GLubyte OverlayRenderer::m_dummyTextureBuffer[3] = {
    0xFF,
    0xFF,
    0xFF
};

OverlayRenderer::OverlayRenderer(QQuickWindow *window, Overlay::ImageProvider &imageProvider)
    :m_window(window),
     m_vbo(nullptr),
     m_ibo(nullptr),
     m_vertexShader(nullptr),
     m_fragmentShader(nullptr),
     m_program(nullptr),
     m_texture(nullptr),
     m_activeTexture(nullptr),
     m_model(),
     m_projection(),
     m_color(),
     m_activeTextureUnit(Overlay::Texture::UNIT_ZERO),
     m_primitives(),
     m_initialized(false),
     m_imageProvider(imageProvider),
     m_textureManager(nullptr)

{
    connect(m_window, &QQuickWindow::afterRendering, this, &OverlayRenderer::onRenderRequested, Qt::DirectConnection);
}

OverlayRenderer::~OverlayRenderer()
{
    //delete m_textureManager;
}

void OverlayRenderer::clearPrimitives()
{
    m_primitives.clear();
}

void OverlayRenderer::storeImages(Overlay::Image *begin, Overlay::Image *end)
{
    m_primitives.imageBegin = begin;
    m_primitives.imageEnd = end;
}

void OverlayRenderer::storeTexts(Overlay::Text *begin, Overlay::Text *end)
{
    m_primitives.textBegin = begin;
    m_primitives.textEnd = end;
}

void OverlayRenderer::storeRectangles(Overlay::Rectangle *begin, Overlay::Rectangle *end)
{
    m_primitives.rectangleBegin = begin;
    m_primitives.rectangleEnd = end;
}

void OverlayRenderer::prepareTextures()
{
    /*
     * Clear the textures used in the previous frame.
     */
    m_textureManager->clear();

    /*
     * Iterate over all the images and upload them as textures to the GPU.
     */
    Overlay::Image* imageIt = nullptr;
    for (imageIt = m_primitives.imageBegin; imageIt != m_primitives.imageEnd; ++imageIt)
    {
        Overlay::ImageData* imageData = m_imageProvider.getImageData(imageIt->id);
        if (imageData == nullptr)
        {
            OL_PRINT_LOG("cannot obtain image data for: %d", imageIt->id);
            //skip this iteration.
            continue;
        }

        const bool uploadSuccess = m_textureManager->upload(imageIt->id, imageData);
        if (!uploadSuccess)
        {
            OL_PRINT_DEBUG("cannot upload texture for: %d", imageIt->id);
            continue;
        }
    }
}

void OverlayRenderer::drawPrimitives()
{

    /*
     * Trigger Qt to render a new frame.
     * Qt will call onRenderRequested.
     */
    m_window->update();
}

void OverlayRenderer::onRenderRequested()
{
    if (!m_initialized)
    {
        initialize();
        m_initialized = true;
    }

    QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
    f->glEnable(GL_DEPTH_TEST);
    f->glClear(GL_DEPTH_BUFFER_BIT);

    Overlay::Rectangle* rectangleIt = nullptr;
    for (rectangleIt = m_primitives.rectangleBegin; rectangleIt != m_primitives.rectangleEnd; ++rectangleIt)
    {
        const bool setupSuccess = setupPipeline(rectangleIt, nullptr);
        if (!setupSuccess)
        {
            OL_PRINT_LOG("Error setting up pipeline for rectangle at x=%d, y=%d", rectangleIt->position.x, rectangleIt->position.y);
        }
        else
        {
            Overlay::drawTexture(*m_vbo, *m_ibo, *m_activeTexture, *m_program);
        }
    }

    //TODO: Andrei: Think where to call this.
    prepareTextures();

    Overlay::Image* imageIt = nullptr;
    for (imageIt = m_primitives.imageBegin; imageIt != m_primitives.imageEnd; ++imageIt)
    {
        const bool setupSuccess = setupPipeline(nullptr, imageIt);
        if (!setupSuccess)
        {
            OL_PRINT_LOG("Error setting up pipeline for image: %d", imageIt->id);
        }
        else
        {
            Overlay::drawTexture(*m_vbo, *m_ibo, *m_activeTexture, *m_program);
        }
    }
}

bool OverlayRenderer::setupPipeline(const Overlay::Rectangle* rect, const Overlay::Image* img)
{
    bool setupSuccess = false;
    setupSuccess = setupVBO();
    setupSuccess = setupSuccess ? setupTextures(img) : false;
    setupSuccess = setupSuccess ? setupUniforms(rect, img) : false;
    setupSuccess = setupSuccess ? setupGPUProgram() : false;

    return setupSuccess;
}

bool OverlayRenderer::setupVBO()
{
    /*
     * Make the vbo accessible to the vertex shader
     * as the first and second attribute.
     */
    m_vbo->bind();
    m_vbo->enableVertexAttribute(0, //index in vertex shader
                                 3, //size of the attribute (3 floats)
                                 5 * sizeof(GLfloat), // stride (byte count) between 2 consecutive position attributes
                                 0 // offset until the first position attribute
                                 );
    m_vbo->enableVertexAttribute(1, //index in the vertex shader
                                 2, //size of texture coordinate attribute (2 floats)
                                 5 * sizeof(GLfloat), //byte count between 2 consecutive texture coordinate attributes
                                 3 * sizeof(GLfloat) // offset until the first texture coordinate
                                 );
    m_vbo->unbind();

    return true;
}

bool OverlayRenderer::setupTextures(const Overlay::Image *img)
{
    if (img == nullptr)
    {
        /*
         * For drawing rectangles, the texture will contain
         * only a single white pixel. This way, we don't need
         * to upload a new fragment shader to the GPU (which
         * takes a lot of time).
         */
        const Overlay::Texture::PixelFormat pf = Overlay::Texture::PF_RGB;
        const int width = 1;
        const int height = 1;

        //TODO: Andrei: Think of a better way to do this (only once!)
        static bool first = true;
        if (first)
        {
            m_texture->bind();
            m_texture->upload(pf, width, height, m_dummyTextureBuffer);
            m_texture->unbind();

            first = false;
        }
        m_activeTexture = m_texture;

        return true;
    }
    else
    {
        /*
         * Get the image data at the corresponding id.
         */
        Overlay::Texture* texture = m_textureManager->getTexture(img->id);
        if (texture == nullptr)
        {
            OL_PRINT_LOG("Texture for %d has not been uploaded.", img->id);
            return false;
        }

        m_activeTexture = texture;

        return true;
    }
}

bool OverlayRenderer::setupUniforms(const Overlay::Rectangle* rect, const Overlay::Image* img)
{
    /*
     * Setup model matrix.
     */
    const Overlay::Position position = (rect != nullptr) ? rect->position : img->position;
    const Overlay::Dimension dimension = (rect != nullptr) ? rect->dimension : img->dimension;
    const float zOffset = (rect != nullptr) ? -1.0f : 0.0f;
    m_model.setToIdentity();

    /*
     * The rectangle in the vbo is at (0,0) - (1,1) coordinates.
     * To render the rectangle at the specified position, the following
     * translation is needed:
     * x: position.X
     * y: window.height - position.Y (because the origin in OpenGL is bottom left,
     * as oposed to top left)
     * z: -1(background) for rectangles, 0(foreground) for images
     */
    m_model.translate(position.x, // x
                      m_window->height() - position.y, // y
                      zOffset // z
                      );

    /*
     * Because the origin in a 2D system is at the top left
     * (as opposed to bottom left in OpenGL), a rotation around the X axis is needed.
     */
    m_model.rotate(180.0f, // angle
                   1.0f, // x
                   0.0f, // y
                   0.0f // z
                   );

    /*
     * Scale the unit rectangle to the specified dimensions.
     */
    m_model.scale(dimension.width, dimension.height);

    /*
     * Setup the active color.
     */
    const uint8_t r = (rect != nullptr) ? rect->color.r : 255;
    const uint8_t g = (rect != nullptr) ? rect->color.g : 255;
    const uint8_t b = (rect != nullptr) ? rect->color.b : 255;
    const uint8_t a = (rect != nullptr) ? rect->color.a : 255;
    m_color.setRed(r);
    m_color.setGreen(g);
    m_color.setBlue(b);
    m_color.setAlpha(a);

    /*
     * Setup active texture unit.
     */
    m_activeTextureUnit = m_activeTexture->getUnit();

    /*
     * Mathematics can be hard. If shit does not work, uncomment.
     * Reference: http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/
     */
//    OL_PRINT_DEBUG("x=%d, y=%d, w=%d, h=%d", position.x, position.y, dimension.width, dimension.height);
//    for (int rowIdx = 0; rowIdx < 4; ++rowIdx)
//    {
//        for (int colIdx = 0; colIdx < 4; ++colIdx)
//        {
//            OL_PRINT_DEBUG("i=%d j=%d val=%lf", rowIdx, colIdx, m_model(rowIdx, colIdx));
//        }
//    }

    return true;
}

bool OverlayRenderer::setupGPUProgram()
{
    /*
     * Upload the uniform variables to the GPU.
     */
    m_program->bind();
    m_program->setProjection(m_projection.data());
    m_program->setModel(m_model.data());
    m_program->setFragmentColor(
                m_color.redF(),
                m_color.greenF(),
                m_color.blueF(),
                m_color.alphaF()
    );
    m_program->setTextureSampler(m_activeTextureUnit);
    m_program->unbind();

    return true;
}

void OverlayRenderer::initialize()
{
    m_vbo = new Overlay::VertexBufferObject(m_vertices, VERTEX_COUNT, VERTEX_SIZE);
    m_ibo = new Overlay::IndexBufferObject(m_indices, INDEX_COUNT, *m_vbo);
    m_vertexShader = new Overlay::Shader(m_vertexShaderCode, Overlay::Shader::T_VERTEX_SHADER, Overlay::GPUProgram::OpenGL_2_ES);
    m_fragmentShader = new Overlay::Shader(m_fragmentShaderCode, Overlay::Shader::T_FRAGMENT_SHADER, Overlay::GPUProgram::OpenGL_2_ES);
    m_program = new Overlay::GPUProgram(Overlay::GPUProgram::OpenGL_2_ES);
    m_texture = new Overlay::Texture(Overlay::Texture::WM_REPEAT, Overlay::Texture::FM_NEAREST, Overlay::Texture::UNIT_ZERO);
    m_textureManager = new Overlay::TextureManager();

    /*
     * The application does not manage the OpenGL context directly.
     * It gets the context from Qt. Tell Qt to initialize the functions.
     */
    QOpenGLContext::currentContext()->functions()->initializeOpenGLFunctions();

    setupVBO();

    /*
     * Create a GPU program from the vertex
     * and fragment shaders.
     */
    m_vertexShader->compile();
    m_fragmentShader->compile();
    m_program->attachShader(*m_vertexShader);
    m_program->attachShader(*m_fragmentShader);
    m_program->link();

    /*
     * Initialize the uniform variables (gpu side).
     */
    m_program->initializeModel("model");
    m_program->initializeProjection("projection");
    m_program->initializeFragmentColor("fragmentColor");
    m_program->initializeTextureSampler("u_sampler");

    /*
     * Setup the variables that will be uploaded
     * to the GPU as uniforms (cpu side).
     */
    m_model.setToIdentity();
    m_projection.setToIdentity();
    m_projection.ortho(0.0f, // left
                       m_window->width(), // right
                       0.0f, // bottom
                       m_window->height(), // top
                       0.0f, // near
                       10.0f // far
                       );
    m_color.setRgba(QRgb(0x000000ff)); // opaque black
}
