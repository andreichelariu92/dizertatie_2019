#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "ImageProvider.h"

using namespace Overlay;

ImageData::ImageData(int width, int height, int pixelDepth, unsigned char *data)
    :m_width(width),
     m_height(height),
     m_pixelDepth(pixelDepth),
     m_data(data)
{
}

ImageData::ImageData(ImageData &&source)
    :m_width(source.m_width),
     m_height(source.m_height),
     m_pixelDepth(source.m_pixelDepth),
     m_data(source.m_data)
{
    source.m_data = nullptr;
}

ImageData& ImageData::operator =(ImageData &&source)
{
    if (m_data)
    {
        stbi_image_free(m_data);
    }

    m_width = source.m_width;
    m_height = source.m_height;
    m_pixelDepth = source.m_pixelDepth;
    m_data = source.m_data;

    source.m_data = nullptr;

    return *this;
}

ImageData::~ImageData()
{
    if (m_data)
    {
        stbi_image_free(m_data);
    }
}

ImageProvider::ImageProvider()
    :m_images()
{

}

uint16_t ImageProvider::readImage(const char * const path)
{
    uint16_t output = UINT16_MAX;

    /*
     * Try to find the image in the vector.
     * If it is already in memory, return an id to it.
     */
    uint16_t imageIdx = 0;
    for (; imageIdx < m_images.size(); ++imageIdx)
    {
        if (m_images[imageIdx].path == path)
        {
            output = imageIdx;
            break;
        }
    }
    if (output != UINT16_MAX)
    {
        return output;
    }

    /*
     * Read the image from the disk.
     * Store it in memory.
     * Return the id of the image (position in vector).
     */
    int w = 0;
    int h = 0;
    int pixelDepth = 0;
    unsigned char* data = stbi_load(path, &w, &h, &pixelDepth, 0);
    if (data != NULL)
    {
        m_images.emplace_back(w, h, pixelDepth, data, path);
        output = m_images.size() - 1;
    }

    return output;
}

ImageData* ImageProvider::getImageData(uint16_t id)
{
    ImageData* output = nullptr;

    if (id < m_images.size())
    {
        output = &(m_images[id].image);
    }
    return output;
}
