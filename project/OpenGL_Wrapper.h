#ifndef OpenGL_Wrapper_INCLUDE_GUARD
#define OpenGL_Wrapper_INCLUDE_GUARD

#include <QOpenGLFunctions>

namespace Overlay
{

    class GPUProgram;

    /**
     * @brief Class encapsulating an OpenGL vertex buffer object.
     */
    class VertexBufferObject
    {
    public:
        enum ErrorStatus
        {
            ES_NO_ERROR = 0,
            ES_MEMORY_ERROR = 1,
            ES_INVALID_DATA = 2
        };

    private:
        GLfloat* m_vertexBegin;
        size_t m_vertexCount;
        size_t m_vertexSize;
        GLuint m_vbo;
        ErrorStatus m_errorStatus;
        bool m_isBinded;

        //remove copy operations
        VertexBufferObject(const VertexBufferObject& source);
        VertexBufferObject& operator=(const VertexBufferObject& source);

        friend void drawArray(VertexBufferObject& vbo, GPUProgram& program);

    public:
        /**
         * @brief Constructs the VBO from the specified buffer.
         * It allocates internally a buffer of vertexCount * vertexSize bytes.
         * The caller is responsible for the memory region pointer by vertexBegin.
         */
        VertexBufferObject(GLfloat* vertexBegin, size_t vertexCount, size_t vertexSize);
        /**
          * @brief Clears up the memory associated with the VBO.
          * Clears also any OpenGL resources needed.
         */
        ~VertexBufferObject();
        /**
         * @brief Makes the VBO the current one.
         */
        void bind();
        /**
         * @brief The VBO will no longer be the current one.
         */
        void unbind();
        /**
         * @brief Enables the values from the VBO to the vertex shader.
         * The values will be accessed by the shader at index.
         */
        void enableVertexAttribute(unsigned int index, int size, int stride, unsigned int byteOffset);
        /**
         * @brief Getter for VBO error status.
         * Check the getter after creating it and before using it.
         */
        VertexBufferObject::ErrorStatus getErrorStatus()const
        {
            return m_errorStatus;
        }
        /**
         * @brief Getter for VBO vertex count.
         */
        size_t getVertexCount()const
        {
            return m_vertexCount;
        }
        /**
         * @brief Getter for VBO vertex size.
         */
        size_t getVertexSize()const
        {
            return m_vertexSize;
        }
        /**
         * @brief Getter to check if VBO is bound.
         */
        bool isBinded()const
        {
            return m_isBinded;
        }
    };

    /**
     * @brief Class encapsulating an OpenGL index buffer object
     * (also known as Element Buffer Object).
     */
    class IndexBufferObject
    {
    public:
        enum ErrorStatus
        {
            ES_NO_ERROR = 0,
            ES_MEMORY_ERROR = 1,
            ES_VBO_NOT_BINDED = 2
        };
        /**
         * @brief Construct an IBO from the buffer pointed at indexBegin.
         * The class will allocate internally a memory region of indexCount
         * unsigned integers. The caller is responsible for the memory region
         * pointed by indexBegin.
         */
        IndexBufferObject(unsigned int* indexBegin, size_t indexCount, VertexBufferObject& vbo);
        /**
         * @brief Deallocates the memory region and any OpenGL resources
         * associated with the IBO.
         */
        ~IndexBufferObject();
        /**
         * @brief Makes the IBO the current one.
         */
        void bind();
        /**
         * @brief Unmarks the IBO as the current one.
         */
        void unbind();
        /**
         * @brief Getter for the error status.
         * The value must be checked after the IBO is created
         * and before any usage.
         */
        ErrorStatus getErrorStatus()const
        {
            return m_errorStatus;
        }
        /**
         * @brief Getter for the index count.
         */
        size_t getIndexCount()const
        {
            return m_indexCount;
        }
    private:
        //remove copy operations
        IndexBufferObject(const IndexBufferObject& source);
        IndexBufferObject& operator=(const IndexBufferObject& source);

        unsigned int * m_indexBegin;
        size_t m_indexCount;
        GLuint m_ibo;
        ErrorStatus m_errorStatus;
        VertexBufferObject& m_vbo;
    };

    class Texture
    {
    public:
        enum ErrorStatus
        {
            ES_NO_ERROR = 0,
            ES_NO_DATA_UPLOADED = 1
        };

        enum WrapMode
        {
            WM_CLAMP_TO_BORDER = 0,
            WM_REPEAT = 1
        };

        enum FilterMode
        {
            FM_NEAREST = 0,
            FM_LINEAR = 1
        };

        enum PixelFormat
        {
            PF_RGB = 0,
            PF_RGBA = 1,
            PF_LAST = 2
        };

        enum Unit
        {
            UNIT_ZERO = 0,
            UNIT_ONE = 1,
            UNIT_TWO = 2,
            UNIT_THREE = 3
        };

        Texture(WrapMode wm, FilterMode fm, Unit unit);
        Texture(const Texture& source) = delete;
        Texture& operator=(const Texture& source) = delete;
        Texture(Texture&& source);
        Texture& operator=(Texture&& source);
        ~Texture();

        void bind();
        void unbind();
        Texture::ErrorStatus getErrorStatus()const
        {
            return m_errorStatus;
        }
        Texture::Unit getUnit()const
        {
            return m_unit;
        }
        void upload(PixelFormat pf, unsigned int width, unsigned int height, void* data);

    private:
        GLuint m_id;
        ErrorStatus m_errorStatus;
        Texture::Unit m_unit;
    };

    class Shader;

    /**
     * @brief Class encapsulating an OpenGL shader program.
     * The program is composed from a vertex shader and a fragment shader.
     * The program will be run on the GPU.
     */
    class GPUProgram
    {
    public:
        enum ErrorStatus
        {
            ES_NO_ERROR = 0,
            ES_CREATION_ERROR = 1,
            ES_DIFFERENT_OPEN_GL_VERSIONS = 2,
            ES_LINKING_ERROR = 3,
            ES_VALIDATION_ERROR = 4,
            ES_PROJECTION_NOT_INITIALIZED = 5,
            ES_MODEL_NOT_INITIALIZED = 6,
            ES_FRAGMENT_COLOR_NOT_INITIALIZED = 7,
            ES_TEXTURE_SAMPLER_NOT_INITIALIZED = 8
        };

        enum OpenGLVersion
        {
            OpenGL_2_ES = 0,
            OpenGL_3_ES = 1
        };

    private:
        GLuint m_id;
        GLint m_modelId;
        GLint m_projectionId;
        GLint m_fragmentColorId;
        ErrorStatus m_errorStatus;
        OpenGLVersion m_openGLVersion;
        GLint m_textureSamplerId;

        //Remove copy operations.
        GPUProgram(const GPUProgram& source);
        GPUProgram& operator=(const GPUProgram& source);

    public:
        /**
         * @brief Create an OpenGL program. The program
         * must be configured before used.
         */
        GPUProgram(OpenGLVersion openGLVersion);
        /**
         * @brief Destroys the OpenGL program.
         */
        ~GPUProgram();
        /**
         * @brief Attach a shader to the program.
         * The shader must be compiled before it is attached.
         * Only a fragment shader and a vertex shader can be
         * attached.
         */
        void attachShader(const Shader& shader);
        /**
         * @brief Link the program. This step also checks for
         * additional errors of the program.
         */
        void link();
        /**
         * @brief Set the name of the projection matrix as it
         * is specified in the vertex shader.
         */
        void initializeProjection(const char* projectionName);
        /**
         * @brief Set the value of the projection matrix.
         * The matrix has 4x4 dimensions.
         */
        void setProjection(const GLfloat* data);
        /**
         * @brief Set the name of the model matrix, as it
         * is specified in the vertex shader.
         */
        void initializeModel(const char* modelName);
        /**
         * @brief Set the value of the model matrix.
         * The matrix has 4x4 dimensions.
         */
        void setModel(const GLfloat* data);
        /**
         * @brief Set the name of the fragment color, as
         * it is specified in the fragment shader.
         */
        void initializeFragmentColor(const char* fragmentColorName);
        /**
         * @brief Set the color of the fragment shader.
         */
        void setFragmentColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a);

        void initializeTextureSampler(const char* textureSampler);

        void setTextureSampler(Texture::Unit unit);

        /**
         * @brief Make the GPU program the current one.
         */
        void bind();
        /**
         * @brief Unmark the GPU program as the current one.
         */
        void unbind();
        /**
         * @brief Getter for the error status of the GPU program.
         * This value must be checked after creating the program
         * and before using it.
         */
        ErrorStatus getErrorStatus()const
        {
            return m_errorStatus;
        }
    };

    /**
     * @brief Class encapsulating an OpenGL shader (vertex or fragment).
     */
    class Shader
    {
    public:
        enum ErrorStatus
        {
            ES_NO_ERROR = 0,
            ES_COMPILATION_ERROR = 1,
            ES_CREATION_ERROR = 2
        };

        enum Type
        {
            T_VERTEX_SHADER = 0,
            T_FRAGMENT_SHADER = 1
        };

    private:
        GLuint m_id;
        Type m_shaderType;
        ErrorStatus m_errorStatus;
        GPUProgram::OpenGLVersion m_openGLVersion;

        //Remove copy operations.
        Shader(const Shader& source);
        Shader& operator=(const Shader& source);

        friend class GPUProgram;
    public:
        /**
         * @brief Creates an OpenGL shader from the specified source code and
         * of the specified type. The OpenGL version must be the same as that
         * of the GPU program. In VP2, the version used is OpenGL ES 2.
         */
        Shader(const char *const sourceCode, Shader::Type type, GPUProgram::OpenGLVersion openGLVersion);
        /**
         * @brief Compiles the shader. The check if the compilation succeeded,
         * check the error status.
         */
        void compile();
        /**
         * @brief Getter for the error status of the OpenGL shader.
         * This values must be checked after creation and compilation
         * of the shader.
         */
        ErrorStatus getErrorStatus()const
        {
            return m_errorStatus;
        }
        /**
         * @brief Destroys the shader and any associated resources.
         */
        ~Shader();
    };

    /**
     * @brief Draws the triangles in the specified VBO.
     * The vertex points will be processed by the vertex shader
     * and fragment shader attached to the GPU program.
     */
    void drawArray(VertexBufferObject& vbo, GPUProgram& program);
    /**
     * @brief Draws the triangles in the specified VBO based on the
     * indices in the IBO. The vertex points will be processed by
     * the shaders attached to the GPU program.
     */
    void drawElements(VertexBufferObject &vbo, IndexBufferObject& ibo, GPUProgram& program);

    void drawTexture(VertexBufferObject& vbo, IndexBufferObject& ibo, Texture& texture, GPUProgram& program);

}// namespace Overlay

#endif
