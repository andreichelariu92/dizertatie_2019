#include <string.h>
#include <stdio.h>

#include "Label.h"
#include "OverlayLog.h"

using namespace Overlay;

EmptyLabel::EmptyLabel(Text *text, Rectangle *rectangle)
    :IOverlayWidget(),
     m_text(text),
     m_rectangle(rectangle)
{
}

EmptyLabel::~EmptyLabel()
{
    /*
     * The memory for the text and the rectangle
     * is managed by the view. No need to delete it.
     */
}

EmptyLabelBuilder::EmptyLabelBuilder(OverlayScreenTemplate &controller)
    :m_label(nullptr),
     m_controller(controller)
{
    /*
     * Try to get a rectangle and a text from the view.
     */

    Text* text = m_controller.getView().allocateText();
    Overlay::Rectangle* rectangle = m_controller.getView().allocateRectangle();
    if (text == nullptr || rectangle == nullptr)
    {
        OL_PRINT_LOG("Cannot create label");
    }

    m_label = new EmptyLabel(text, rectangle);
}

EmptyLabelBuilder::~EmptyLabelBuilder()
{
    /*
     * Do nothing. The controller will be responsible
     * for deleting the label.
     */
}

void EmptyLabelBuilder::setText(const char *text)
{
    if (m_label == nullptr)
    {
        return;
    }

    const uint8_t textLength = strlen(text) + 1;
    if (textLength > MAX_STRING_LENGTH)
    {
        OL_PRINT_LOG("Cannot copy the whole text into the label.");
        return;
    }

    strcpy(m_label->m_text->string.data, text);
    m_label->m_text->string.count = textLength;
}

void EmptyLabelBuilder::setBackColor(const Color &color)
{
    if (m_label == nullptr)
    {
        return;
    }

    m_label->m_rectangle->color = color;
}

void EmptyLabelBuilder::setTextColor(const Color &color)
{
    if (m_label == nullptr)
    {
        return;
    }

    m_label->m_text->color = color;
}

void EmptyLabelBuilder::setPosition(int x, int y)
{
    if (m_label == nullptr)
    {
        return;
    }

    /*
     * The rectangle and the text will have the same
     * position and the same dimensions. The color of
     * the surrounding the text will be transparent,
     * meaning the color of the rectangle will be the one
     * surrounding the text.
     */
    m_label->m_rectangle->position.x = x;
    m_label->m_rectangle->position.y = y;
    m_label->m_text->position.x = x;
    m_label->m_text->position.y = y;
}

void EmptyLabelBuilder::setDimension(int w, int h)
{
    if (m_label == nullptr)
    {
        return;
    }

    /*
     * The logic is the same as in setPosition().
     */
    m_label->m_rectangle->dimension.width = w;
    m_label->m_rectangle->dimension.height = h;
    m_label->m_text->dimension.width = w;
    m_label->m_text->dimension.height = h;
}

Overlay::EmptyLabel* EmptyLabelBuilder::buildLabel()
{
    return m_label;
}
