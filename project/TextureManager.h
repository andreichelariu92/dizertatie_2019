#ifndef TextureManager_INCLUDE_GUARD
#define TextureManager_INCLUDE_GUARD

#include <map>

#include <OpenGL_Wrapper.h>
#include <ImageProvider.h>

namespace Overlay
{
    class TextureManager
    {
    private:
        std::map<uint16_t, Overlay::Texture> m_textures;
    public:
        TextureManager();
        //remove copy operations
        TextureManager(const TextureManager& source) = delete;
        TextureManager& operator=(const TextureManager& source) = delete;
        //remove move operations
        TextureManager(TextureManager&& source) = delete;
        TextureManager& operator=(TextureManager&& soruce) = delete;
        ~TextureManager();

        bool upload(uint16_t id, const Overlay::ImageData* imageData);
        Overlay::Texture* getTexture(uint16_t id);
        void clear();
    };
}

#endif
