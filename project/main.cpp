#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>

#include "OverlayRenderer.h"
#include "OverlayManager.h"
#include "ImageProvider.h"

using namespace Overlay;

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQuickView window;
    window.setWidth(1280);
    window.setHeight(768);
    window.setResizeMode(QQuickView::SizeRootObjectToView);
    window.setSource(QUrl(QStringLiteral("qrc:/main.qml")));
    window.setTitle("Demo Application");
    window.setOpacity(0.999);
    window.setColor(Qt::transparent);

    ImageProvider imageProvider;
    OverlayRenderer renderer(&window, imageProvider);
    OverlayManager manager(renderer, imageProvider);

    window.show();
    manager.showOverlay(Overlay::TEST_OVERLAY);

    return app.exec();
}
