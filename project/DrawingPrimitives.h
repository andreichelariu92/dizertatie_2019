#ifndef DrawingPrimitives_INCLUDE_GUARD
#define DrawingPrimitives_INCLUDE_GUARD

#include <stdint.h>

#define MAX_STRING_LENGTH 16

namespace Overlay
{

struct Position
{
    uint16_t x;
    uint16_t y;

    Position() = default;
    Position(uint16_t argX, uint16_t argY)
        :x(argX),
         y(argY)
    {
    }
    Position(const Position& source) = default;
    Position& operator=(const Position& source) = default;
    Position(Position&& source) = default;
    Position& operator=(Position&& source) = default;
    ~Position() = default;
};

struct Dimension
{
    uint16_t width;
    uint16_t height;

    Dimension() = default;
    Dimension(uint16_t w, uint16_t h)
        :width(w),
         height(h)
    {
    }
    Dimension(const Dimension& source) = default;
    Dimension& operator=(const Dimension& source) = default;
    Dimension(Dimension&& source) = default;
    Dimension& operator=(Dimension&& source) = default;
    ~Dimension() = default;
};

struct Color
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;

    Color();
    Color(uint8_t argR, uint8_t argG, uint8_t argB, uint8_t argA = 255)
        :r(argR),
         g(argG),
         b(argB),
         a(argA)
    {
    }
    Color(const Color& source) = default;
    Color& operator=(const Color& source) = default;
    Color(Color&& source) = default;
    Color& operator=(Color&& source) = default;
    ~Color() = default;
};


struct String
{
    char data[MAX_STRING_LENGTH + 1];
    uint8_t count;

    /*
     * The data in the string is not initialized.
     */
    String() = default;
    String(const char* argData);
    String(const String& source) = default;
    String& operator=(const String& source) = default;
    String(String&& source) = default;
    String& operator=(String&& source) = default;
    ~String() = default;
};

struct Image
{
    Position position;
    Dimension dimension;
    uint16_t id;

    Image() = default;
    Image(const Image& source) = default;
    Image& operator=(const Image& source) = default;
    Image(Image&& source) = default;
    Image& operator=(Image&& source) = default;
    ~Image() = default;
};

struct Text
{
    Position position;
    Dimension dimension;
    String string;
    Color color;

    Text() = default;
    Text(const Text& source) = default;
    Text& operator=(const Text& source) = default;
    Text(Text&& source) = default;
    Text& operator=(Text&& source) = default;
    ~Text() = default;
};

struct Rectangle
{
    Position position;
    Dimension dimension;
    Color color;

    Rectangle() = default;
    Rectangle(const Rectangle& source) = default;
    Rectangle& operator=(const Rectangle& source) = default;
    Rectangle(Rectangle&& source) = default;
    Rectangle& operator=(Rectangle&& source) = default;
    ~Rectangle() = default;
};

} // namespace Overlay

#endif
