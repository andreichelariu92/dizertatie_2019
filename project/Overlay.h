#ifndef Overlay_INCLUDE_GUARD
#define Overlay_INCLUDE_GUARD

#include "DrawingPrimitives.h"
#include "PrimitiveStore.h"
#include "OverlayRenderer.h"

#define MAX_IMAGES 10
#define MAX_TEXTS 10
#define MAX_RECTANGLES 10

class PrimitiveStore
{
private:
    ObjectStore<Overlay::Image, MAX_IMAGES> m_imageStore;
    ObjectStore<Overlay::Text, MAX_TEXTS> m_textStore;
    ObjectStore<Overlay::Rectangle, MAX_RECTANGLES> m_rectangleStore;

    Overlay::OverlayRenderer& m_renderer;
public:
    PrimitiveStore(Overlay::OverlayRenderer& renderer);
    PrimitiveStore(const PrimitiveStore& source) = delete;
    PrimitiveStore& operator=(const PrimitiveStore& source) = delete;
    PrimitiveStore(PrimitiveStore&& source) = delete;
    PrimitiveStore& operator=(PrimitiveStore&& source) = delete;
    ~PrimitiveStore();

    Overlay::Image* allocateImage();
    Overlay::Text* allocateText();
    Overlay::Rectangle* allocateRectangle();

    void hidePrimitives();
    void drawPrimitives();
};



#endif
