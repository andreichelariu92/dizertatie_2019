#include "OverlayLog.h"
#include "Icon.h"

using namespace Overlay;

Icon::Icon(Overlay::Image *image)
    :IOverlayWidget(),
     m_image(image)
{
}

Icon::~Icon()
{
    /*
     * Do nothing. The memory for the image
     * is managed by the parent view and it
     * will be deleted when the view is deleted.
     */
}

IconBuilder::IconBuilder(OverlayScreenTemplate &controller)
    :m_icon(nullptr)
{
    Overlay::Image* img = controller.getView().allocateImage();
    if (img == nullptr)
    {
        OL_PRINT_LOG("Cannot allocate memory of icon");
    }
    else
    {
        m_icon = new Overlay::Icon(img);
    }
}

void IconBuilder::setDimension(uint16_t w, uint16_t h)
{
    if (m_icon)
    {
        m_icon->m_image->dimension.width = w;
        m_icon->m_image->dimension.height = h;
    }
}

void IconBuilder::setPosition(uint16_t x, uint16_t y)
{
    if (m_icon)
    {
        m_icon->m_image->position.x = x;
        m_icon->m_image->position.y = y;
    }
}

void IconBuilder::setSource(const char * const source, ImageProvider &provider)
{
    uint16_t id = provider.readImage(source);
    if (id != UINT16_MAX && m_icon)
    {
        m_icon->m_image->id = id;
    }
    else
    {
        OL_PRINT_DEBUG("Cannot find image at :%s", source);
    }
}

Overlay::Icon* IconBuilder::buildIcon()
{
    return m_icon;
}

IconBuilder::~IconBuilder()
{
    /*
     * The icon will be deleted when the view is deleted.
     */
}
