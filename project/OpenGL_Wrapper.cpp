#include <stddef.h>
#include <string.h>

#include <new>

#include "OpenGL_Wrapper.h"
#include "OverlayLog.h"

using namespace Overlay;

/*******************************************************************************
 * VertexBufferObject class methods
*******************************************************************************/
VertexBufferObject::VertexBufferObject(GLfloat* vertexBegin, size_t vertexCount, size_t vertexSize)
    :m_vertexBegin(NULL),
     m_vertexCount(vertexCount),
     m_vertexSize(vertexSize),
     m_vbo(0),
     m_errorStatus(VertexBufferObject::ES_NO_ERROR),
     m_isBinded(false)
{
    if (vertexBegin == NULL)
    {
        m_errorStatus = VertexBufferObject::ES_INVALID_DATA;
    }
    else
    {
        //Allocate memory.
        const size_t floatCount = m_vertexCount * m_vertexSize;
        const size_t byteCount = floatCount * sizeof(GLfloat);
        m_vertexBegin = new (std::nothrow) GLfloat[floatCount];

        if (m_vertexBegin == NULL)
        {
            m_errorStatus = VertexBufferObject::ES_MEMORY_ERROR;
        }
        else
        {
            //Copy buffer.
            memcpy(m_vertexBegin, vertexBegin, byteCount);

            //Create an OpenGL VBO and associate the data to it.
            QOpenGLContext* ctx = QOpenGLContext::currentContext();
            QOpenGLFunctions* f = ctx->functions();
            f->glGenBuffers(1, &m_vbo);
            f->glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
            f->glBufferData(GL_ARRAY_BUFFER, byteCount, m_vertexBegin, GL_DYNAMIC_DRAW);
            f->glBindBuffer(GL_ARRAY_BUFFER, 0);
        }
    }
}

VertexBufferObject::VertexBufferObject(const VertexBufferObject& source)
{
    //not used
}

VertexBufferObject& VertexBufferObject::operator=(const VertexBufferObject& source)
{
    //not used
    return *this;
}

VertexBufferObject::~VertexBufferObject()
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        delete[] m_vertexBegin;
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        f->glDeleteBuffers(1, &m_vbo);
    }
}

void VertexBufferObject::bind()
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        f->glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
        m_isBinded = true;
    }
}

void VertexBufferObject::unbind()
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        f->glBindBuffer(GL_ARRAY_BUFFER, 0);
        m_isBinded = false;
    }
}

void VertexBufferObject::enableVertexAttribute(unsigned int index, int size, int stride, unsigned int byteOffset)
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        //OpenGL requires the address to be specified as a void pointer.
        GLvoid* offsetAddress = NULL;
        if (byteOffset != 0)
        {
            offsetAddress = reinterpret_cast<GLvoid*>(byteOffset);
        }

        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        f->glVertexAttribPointer(index,
                              size,
                              GL_FLOAT,
                              GL_FALSE,
                              stride,
                              offsetAddress);
        f->glEnableVertexAttribArray(index);
    }
}
/*******************************************************************************
 * IndexBufferObject class methods
*******************************************************************************/
IndexBufferObject::IndexBufferObject(unsigned int *indexBegin, size_t indexCount, VertexBufferObject& vbo)
    :m_indexBegin(NULL),
     m_indexCount(indexCount),
     m_ibo(0),
     m_errorStatus(IndexBufferObject::ES_NO_ERROR),
     m_vbo(vbo)
{
    //Allocate memory.
    m_indexBegin = new unsigned int[indexCount];
    if (m_indexBegin == NULL)
    {
        m_errorStatus = IndexBufferObject::ES_MEMORY_ERROR;
    }
    else
    {
        //Copy indexes to new memory
        const size_t byteCount = m_indexCount * sizeof(GLfloat);
        memcpy(m_indexBegin, indexBegin, byteCount);

        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        f->glGenBuffers(1, &m_ibo);
        f->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
        f->glBufferData(GL_ELEMENT_ARRAY_BUFFER, static_cast<GLsizeiptr>(byteCount), m_indexBegin, GL_STATIC_DRAW);
        f->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
}

IndexBufferObject::~IndexBufferObject()
{
    if (m_errorStatus != IndexBufferObject::ES_MEMORY_ERROR)
    {
        delete[] m_indexBegin;
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        f->glDeleteBuffers(1, &m_ibo);
    }
}

IndexBufferObject::IndexBufferObject(const IndexBufferObject &source)
    :m_indexBegin(NULL),
     m_indexCount(0),
     m_ibo(0),
     m_errorStatus(IndexBufferObject::ES_NO_ERROR),
     m_vbo(source.m_vbo)
{
    //not used
}

IndexBufferObject&  IndexBufferObject::operator =(const IndexBufferObject& source)
{
    //not used
    return *this;
}

void IndexBufferObject::bind()
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        //The IBO needs a bounded ibo for it to work.
        if (!m_vbo.isBinded())
        {
            m_errorStatus = IndexBufferObject::ES_VBO_NOT_BINDED;
        }
        else
        {
            QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
            f->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
        }
    }
}

void IndexBufferObject::unbind()
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        f->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
}


/*******************************************************************************
 * Shader class methods
*******************************************************************************/
Shader::Shader(const char *const sourceCode, Shader::Type type, GPUProgram::OpenGLVersion openGLVersion)
    :m_id(0),
     m_shaderType(type),
     m_errorStatus(Shader::ES_NO_ERROR),
     m_openGLVersion(openGLVersion)
{
    //Create shader object on GPU.
    QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
    m_id = f->glCreateShader((m_shaderType == Shader::T_VERTEX_SHADER) ? GL_VERTEX_SHADER : GL_FRAGMENT_SHADER);
    if (m_id == 0)
    {
        m_errorStatus = Shader::ES_CREATION_ERROR;
    }
    else
    {
        //Associate the source code with the shader.
        const GLchar* codeChunks[1];
        codeChunks[0] = sourceCode;
        GLint chunkSizes[1];
        chunkSizes[0] = static_cast<int>(strlen(sourceCode));
        f->glShaderSource(m_id, 1, codeChunks, chunkSizes);
    }
}

Shader::Shader(const Shader& source)
{
    //not used
}

Shader& Shader::operator=(const Shader& source)
{
    //not used
    return *this;
}

Shader::~Shader()
{
    if (m_errorStatus != Shader::ES_CREATION_ERROR)
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        f->glDeleteShader(m_shaderType);
    }
}

void Shader::compile()
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();

        //Try to compile the shader.
        f->glCompileShader(m_id);

        //Check for errors.
        GLint success;
        GLchar errorMessage[1024] = {0};
        f->glGetShaderiv(m_id, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            f->glGetProgramInfoLog(m_id, 1024, NULL, errorMessage);
            OL_PRINT_LOG("Error compiling shader type : %s message: %s",
                         ((m_shaderType == T_VERTEX_SHADER) ? "vertex" : "fragment"),
                         errorMessage);
            m_errorStatus = Shader::ES_COMPILATION_ERROR;
        }
    }
}

/*******************************************************************************
 * Texture class methods
*******************************************************************************/
Texture::Texture(WrapMode wm, FilterMode fm, Unit unit)
    :m_id(0),
     m_errorStatus(Texture::ES_NO_DATA_UPLOADED),
     m_unit(unit)
{
    QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();

    /*
     * Generate buffer id.
     */
    f->glGenTextures(1, &m_id);
    /*
     * Setup wrap properties.
     */
    f->glBindTexture(GL_TEXTURE_2D, m_id);
    switch (wm)
    {
    case Texture::WM_REPEAT:
        f->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        f->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        break;
    case Texture::WM_CLAMP_TO_BORDER:
        f->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        f->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        break;
    }
    /*
     * Setup filter properties.
     */
    switch (fm)
    {
    case Texture::FM_LINEAR:
        f->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        f->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        break;
    case Texture::FM_NEAREST:
        f->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        f->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        break;
    }
}

Texture::~Texture()
{
    QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
    if (m_id != 0)
    {
        f->glDeleteTextures(1, &m_id);
    }
}

Texture::Texture(Texture&& source)
    :m_id(source.m_id),
     m_errorStatus(source.m_errorStatus),
     m_unit(source.m_unit)
{
    //For OpenGL, 0 means no texture object.
    source.m_id = 0;
}

Texture& Texture::operator =(Texture&& source)
{
    if (m_id != 0)
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        f->glDeleteTextures(1, &m_id);
    }

    m_id = source.m_id;
    m_errorStatus = source.m_errorStatus;
    m_unit = source.m_unit;

    source.m_id = 0;

    return *this;
}

void Texture::bind()
{
    if (m_errorStatus != Texture::ES_NO_ERROR && m_errorStatus != Texture::ES_NO_DATA_UPLOADED)
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        /*
         * Activate the proper texture unit.
         */
        switch (m_unit)
        {
        case Texture::UNIT_ZERO:
            f->glActiveTexture(GL_TEXTURE0);
            break;
        case Texture::UNIT_ONE:
            f->glActiveTexture(GL_TEXTURE1);
            break;
        case Texture::UNIT_TWO:
            f->glActiveTexture(GL_TEXTURE2);
            break;
        case Texture::UNIT_THREE:
            f->glActiveTexture(GL_TEXTURE3);
            break;
        }
        /*
         * Bind the texture to the active texture unit.
         */
        f->glBindTexture(GL_TEXTURE_2D, m_id);
    }
}

void Texture::unbind()
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        /*
         * Activate the proper texture unit.
         */
        switch (m_unit)
        {
        case Texture::UNIT_ZERO:
            f->glActiveTexture(GL_TEXTURE0);
            break;
        case Texture::UNIT_ONE:
            f->glActiveTexture(GL_TEXTURE1);
            break;
        case Texture::UNIT_TWO:
            f->glActiveTexture(GL_TEXTURE2);
            break;
        case Texture::UNIT_THREE:
            f->glActiveTexture(GL_TEXTURE3);
            break;
        }
        /*
         * Unbind the texture of the active texture unit.
         */
        f->glBindTexture(GL_TEXTURE_2D, 0);
    }
}

void Texture::upload(PixelFormat pf, unsigned int width, unsigned int height, void *data)
{
    if (m_errorStatus != Texture::ES_NO_DATA_UPLOADED && m_errorStatus != Texture::ES_NO_ERROR)
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        /*
         * Activate the proper texture unit.
         */
        switch (m_unit)
        {
        case Texture::UNIT_ZERO:
            f->glActiveTexture(GL_TEXTURE0);
            break;
        case Texture::UNIT_ONE:
            f->glActiveTexture(GL_TEXTURE1);
            break;
        case Texture::UNIT_TWO:
            f->glActiveTexture(GL_TEXTURE2);
            break;
        case Texture::UNIT_THREE:
            f->glActiveTexture(GL_TEXTURE3);
            break;
        }
        /*
         * Bind the texture to the active texture unit.
         */
        f->glBindTexture(GL_TEXTURE_2D, m_id);
        /*
         * Upload the data on the GPU
         */
        GLint format = (pf == Texture::PF_RGB) ? GL_RGB : GL_RGBA;
        f->glTexImage2D(GL_TEXTURE_2D,
                        0, //mip map level
                        format,
                        width,
                        height,
                        0, //border
                        format,
                        GL_UNSIGNED_BYTE,
                        data);
        f->glGenerateMipmap(GL_TEXTURE_2D);

        /*
         * Mark the texture as valid.
         */
        m_errorStatus = Texture::ES_NO_ERROR;
    }
}

/*******************************************************************************
 * GPUProgram class methods
*******************************************************************************/
GPUProgram::GPUProgram(OpenGLVersion version)
    :m_id(0),
     m_modelId(-1),
     m_projectionId(-1),
     m_fragmentColorId(-1),
     m_errorStatus(GPUProgram::ES_NO_ERROR),
     m_openGLVersion(version),
     m_textureSamplerId(-1)
{
    QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
    m_id = f->glCreateProgram();
    if (m_id == 0)
    {
        m_errorStatus = GPUProgram::ES_CREATION_ERROR;
    }
}

GPUProgram::GPUProgram(const GPUProgram& source)
{
    //Not used.
}

GPUProgram& GPUProgram::operator=(const GPUProgram& source)
{
    //Not used.
    return *this;
}

GPUProgram::~GPUProgram()
{
    if (m_errorStatus != GPUProgram::ES_CREATION_ERROR)
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        f->glDeleteProgram(m_id);
    }
}

void GPUProgram::attachShader(const Shader& shader)
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        if (m_openGLVersion != shader.m_openGLVersion)
        {
            m_errorStatus = ES_DIFFERENT_OPEN_GL_VERSIONS;
        }
        else
        {
            QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
            f->glAttachShader(m_id, shader.m_id);
        }
    }
}

void GPUProgram::link()
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();

        //Try to link the program.
        f->glLinkProgram(m_id);

        //Check if linking was successful.
        GLint success = 0;
        GLchar errorMessage[1024] = { 0 };
        f->glGetProgramiv(m_id, GL_LINK_STATUS, &success);
        if (!success)
        {
            f->glGetProgramInfoLog(m_id, sizeof(errorMessage), NULL, errorMessage);
            m_errorStatus = ES_LINKING_ERROR;
            OL_PRINT_LOG("Linking error for shaders: %s", errorMessage);
        }
        else
        {
            //Try to validate the program.
            f->glValidateProgram(m_id);

            //Check if validation was successful.
            f->glGetProgramiv(m_id, GL_VALIDATE_STATUS, &success);
            if (!success)
            {
                f->glGetProgramInfoLog(m_id, sizeof(errorMessage), NULL, errorMessage);
                m_errorStatus = ES_VALIDATION_ERROR;
                OL_PRINT_ERROR_STATUS(m_errorStatus);
            }
        }
    }
}

void GPUProgram::initializeProjection(const char* projectionName)
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        //Try to get the projection uniform location.
        m_projectionId = f->glGetUniformLocation(m_id, projectionName);
        if (m_projectionId == -1)
        {
            m_errorStatus = GPUProgram::ES_PROJECTION_NOT_INITIALIZED;
        }
    }
}

void GPUProgram::setProjection(const GLfloat* data)
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        if (m_projectionId == -1)
        {
            m_errorStatus = ES_PROJECTION_NOT_INITIALIZED;
        }
        else
        {
            QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
            f->glUniformMatrix4fv(m_projectionId, 1, GL_FALSE, data);
        }
    }
}

void GPUProgram::initializeModel(const char* modelName)
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();

        //Try to get the model location.
        m_modelId = f->glGetUniformLocation(m_id, modelName);
        if (m_modelId == -1)
        {
            m_errorStatus = GPUProgram::ES_MODEL_NOT_INITIALIZED;
        }
    }
}

void GPUProgram::setModel(const GLfloat* data)
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        //Check if model was set.
        if (m_modelId == -1)
        {
            m_errorStatus = ES_MODEL_NOT_INITIALIZED;
        }
        else
        {
            QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
            f->glUniformMatrix4fv(m_modelId, 1, GL_FALSE, data);
        }
    }
}

void GPUProgram::initializeFragmentColor(const char* fragmentColorName)
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        m_fragmentColorId = f->glGetUniformLocation(m_id, fragmentColorName);
        if (m_fragmentColorId == -1)
        {
            m_errorStatus = GPUProgram::ES_FRAGMENT_COLOR_NOT_INITIALIZED;
        }
    }
}

void GPUProgram::setFragmentColor(GLfloat r, GLfloat g, GLfloat b, GLfloat a)
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        if (m_fragmentColorId == -1)
        {
            m_errorStatus = ES_FRAGMENT_COLOR_NOT_INITIALIZED;
        }
        else
        {
            QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
            f->glUniform4f(m_fragmentColorId, r, g, b, a);
        }
    }
}

void GPUProgram::initializeTextureSampler(const char* textureSampler)
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        m_textureSamplerId = f->glGetUniformLocation(m_id, textureSampler);
        if (m_textureSamplerId == -1)
        {
            m_errorStatus = GPUProgram::ES_TEXTURE_SAMPLER_NOT_INITIALIZED;
        }
    }
}

void GPUProgram::setTextureSampler(Texture::Unit unit)
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        if (m_textureSamplerId == -1)
        {
            m_errorStatus = GPUProgram::ES_TEXTURE_SAMPLER_NOT_INITIALIZED;
        }
        else
        {
            QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
            int samplerId = 0;
            switch (unit)
            {
            case Texture::UNIT_ZERO:
                samplerId = 0;
                break;
            case Texture::UNIT_ONE:
                samplerId = 1;
                break;
            case Texture::UNIT_TWO:
                samplerId = 2;
                break;
            case Texture::UNIT_THREE:
                samplerId = 3;
                break;
            }

            f->glUniform1i(m_textureSamplerId, samplerId);
        }
    }
}

void GPUProgram::bind()
{
    if (!OL_IS_VALID(m_errorStatus))
    {
       OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        f->glUseProgram(m_id);
    }
}

void GPUProgram::unbind()
{
    if (!OL_IS_VALID(m_errorStatus))
    {
        OL_PRINT_ERROR_STATUS(m_errorStatus);
    }
    else
    {
        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        f->glUseProgram(0);
    }
}

void Overlay::drawArray(VertexBufferObject& vbo, GPUProgram& program)
{
    if (!OL_IS_VALID_OBJECT(vbo) || !OL_IS_VALID_OBJECT(program))
    {
        OL_PRINT_LOG("invalid vbo or gpu program");
    }
    else
    {
        program.bind();
        vbo.bind();

        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        f->glDrawArrays(GL_TRIANGLES, 0, static_cast<GLsizei>(vbo.getVertexCount()));

        vbo.unbind();
        program.unbind();
    }
}

void Overlay::drawElements(VertexBufferObject &vbo, IndexBufferObject& ibo, GPUProgram& program)
{
    if (!OL_IS_VALID_OBJECT(vbo) || !OL_IS_VALID_OBJECT(ibo) || !OL_IS_VALID_OBJECT(program))
    {
        OL_PRINT_LOG("invalid vbo or ibo or gpu program");
    }
    else
    {
        program.bind();
        vbo.bind();
        ibo.bind();

        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        f->glDrawElements(GL_TRIANGLES, ibo.getIndexCount(), GL_UNSIGNED_INT, NULL);

        ibo.unbind();
        vbo.unbind();
        program.unbind();
    }
}

void Overlay::drawTexture(VertexBufferObject &vbo, IndexBufferObject &ibo, Texture &texture, GPUProgram &program)
{
    if (!OL_IS_VALID_OBJECT(vbo) || !OL_IS_VALID_OBJECT(ibo) || !OL_IS_VALID_OBJECT(texture) || !OL_IS_VALID_OBJECT(program))
    {
      OL_PRINT_LOG("invalid parameters");
    }
    else
    {
        program.bind();
        vbo.bind();
        ibo.bind();
        texture.bind();

        QOpenGLFunctions* f = QOpenGLContext::currentContext()->functions();
        f->glDrawElements(GL_TRIANGLES, ibo.getIndexCount(), GL_UNSIGNED_INT, NULL);

        texture.unbind();
        ibo.unbind();
        vbo.unbind();
        program.unbind();
    }
}
