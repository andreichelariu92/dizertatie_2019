#ifndef OverlayControllerTemplate_INCLUDE_GUARD
#define OverlayControllerTemplate_INCLUDE_GUARD

#include <vector>

#include "Overlay.h"

namespace Overlay
{

class IOverlayWidget
{
public:
    virtual ~IOverlayWidget()
    {
    }
};

class OverlayScreenTemplate
{
private:
    PrimitiveStore m_view;
    std::vector<IOverlayWidget*> m_widgets;
protected:
    virtual void onShown() = 0;
    virtual void onHidden() = 0;
public:
    OverlayScreenTemplate(OverlayRenderer& renderer);
    OverlayScreenTemplate(const OverlayScreenTemplate& source) = delete;
    OverlayScreenTemplate& operator=(const OverlayScreenTemplate& source) = delete;
    OverlayScreenTemplate(OverlayScreenTemplate&& source) = delete;
    OverlayScreenTemplate& operator=(OverlayScreenTemplate&& source) = delete;
    virtual ~OverlayScreenTemplate();

    void show();
    void hide();
    void addWidget(IOverlayWidget* w);
    void clearWidgets();

    PrimitiveStore& getView()
    {
        return m_view;
    }
};

} //namespace Overlay

#endif
