#ifndef OverlayRenderer_INCLUDE_GUARD
#define OverlayRenderer_INCLUDE_GUARD

#include <QQuickWindow>
#include <QMatrix4x4>
#include <QColor>

#include "DrawingPrimitives.h"
#include "OverlayLog.h"
#include "OpenGL_Wrapper.h"
#include "ImageProvider.h"
#include "TextureManager.h"

#define VERTEX_COUNT 4
#define VERTEX_SIZE 5
#define INDEX_COUNT 6

namespace Overlay
{

class OverlayRenderer : public QObject
{
private:
    struct PrimitiveList
    {
        Overlay::Image* imageBegin;
        Overlay::Image* imageEnd;
        Overlay::Text* textBegin;
        Overlay::Text* textEnd;
        Overlay::Rectangle* rectangleBegin;
        Overlay::Rectangle* rectangleEnd;

        PrimitiveList()
            :imageBegin(nullptr),
             imageEnd(nullptr),
             textBegin(nullptr),
             textEnd(nullptr),
             rectangleBegin(nullptr),
             rectangleEnd(nullptr)
        {
        }

        void clear()
        {
            imageBegin = nullptr;
            imageEnd = nullptr;
            textBegin = nullptr;
            textEnd = nullptr;
            rectangleBegin = nullptr;
            rectangleEnd = nullptr;
        }
    };

    QQuickWindow* m_window;
    Overlay::VertexBufferObject* m_vbo;
    Overlay::IndexBufferObject* m_ibo;
    Overlay::Shader* m_vertexShader;
    Overlay::Shader* m_fragmentShader;
    Overlay::GPUProgram* m_program;
    //TODO: Andrei: Rename this to dummy texture.
    Overlay::Texture* m_texture;
    Overlay::Texture* m_activeTexture;
    QMatrix4x4 m_model;
    QMatrix4x4 m_projection;
    QColor m_color;
    Overlay::Texture::Unit m_activeTextureUnit;

    PrimitiveList m_primitives;
    bool m_initialized;
    Overlay::ImageProvider& m_imageProvider;
    Overlay::TextureManager* m_textureManager;

    void prepareTextures();
    bool setupVBO();
    bool setupUniforms(const Overlay::Rectangle* rect, const Overlay::Image* img);
    bool setupGPUProgram();
    bool setupTextures(const Overlay::Image* img);
    bool setupPipeline(const Overlay::Rectangle* rect, const Overlay::Image* img);

    void initialize();

    static const char* m_vertexShaderCode;
    static const char* m_fragmentShaderCode;
    static GLfloat m_vertices[VERTEX_COUNT * VERTEX_SIZE];
    static unsigned int m_indices[INDEX_COUNT];
    static GLubyte m_dummyTextureBuffer[3];

public slots:
    void onRenderRequested();

public:
    OverlayRenderer(QQuickWindow* window, Overlay::ImageProvider& imageProvider);
    OverlayRenderer(const OverlayRenderer& source) = delete;
    OverlayRenderer& operator=(const OverlayRenderer& source) = delete;
    OverlayRenderer(OverlayRenderer&& source) = delete;
    OverlayRenderer& operator=(OverlayRenderer&& source) = delete;
    ~OverlayRenderer();

    void clearPrimitives();
    void storeImages(Overlay::Image* begin, Overlay::Image* end);
    void storeTexts(Overlay::Text* begin, Overlay::Text* end);
    void storeRectangles(Overlay::Rectangle* begin, Overlay::Rectangle* end);
    void drawPrimitives();
};

} // namespace Overlay

#endif
