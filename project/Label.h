#ifndef Label_INCLUDE_GUARD
#define Label_INCLUDE_GUARD

#include "OverlayControllerTemplate.h"

namespace Overlay
{

class EmptyLabelBuilder;

class EmptyLabel : public IOverlayWidget
{
private:
    /*
     * The label does not own the text and the rectangle.
     * They are held by the OverlayView in a contiguous memory region.
     * This improves memory locality and helps rendering operations.
     */
    Overlay::Text* m_text;
    Overlay::Rectangle* m_rectangle;

    /*
     * Inhibit direct creation. This will be done via the builder class.
     */
    EmptyLabel(Overlay::Text* text, Overlay::Rectangle* rectangle);
    friend class EmptyLabelBuilder;
public:
    EmptyLabel(const EmptyLabel& source) = delete;
    EmptyLabel& operator=(const EmptyLabel& source) = delete;
    EmptyLabel(EmptyLabel&& source) = delete;
    EmptyLabel& operator=(EmptyLabel&& source) = delete;
    virtual ~EmptyLabel();

    Overlay::Text* getText()
    {
        return m_text;
    }

    Overlay::Rectangle* getRectangle()
    {
        return m_rectangle;
    }
};

class EmptyLabelBuilder
{
private:
    EmptyLabel* m_label;
    Overlay::OverlayScreenTemplate& m_controller;
public:
    EmptyLabelBuilder(Overlay::OverlayScreenTemplate& controller);
    EmptyLabelBuilder(const EmptyLabelBuilder& source) = delete;
    EmptyLabelBuilder& operator=(const EmptyLabelBuilder& source) = delete;
    EmptyLabelBuilder(EmptyLabelBuilder&& source) = delete;
    EmptyLabelBuilder& operator=(EmptyLabelBuilder&& source) = delete;
    ~EmptyLabelBuilder();

    void setText(const char* text);
    void setBackColor(const Overlay::Color& color);
    void setTextColor(const Overlay::Color& color);
    void setPosition(int x, int y);
    void setDimension(int w, int h);

    EmptyLabel* buildLabel();
};

} //namespace Overlay

#endif
